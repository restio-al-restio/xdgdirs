2021-12-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 2.3

	* configure.ac (AC_INIT): Bump version to "2.3" for release.

2021-12-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add support for ‘--lang python’

	* xdgdirs.in (weirdness): Add entry for ‘python’.
	* xdgdirs.texi (Invoking xdgdirs): Add ‘@item python’.
	* v07: New file.
	* Makefile.am (TESTS): Add v07.

2021-12-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Update GPLv3+ URL in ‘--version’ output

	* xdgdirs.in <check-hv>: ...here.

2021-12-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Use https in freedesktop.org URL

	* xdgdirs.texi (Introduction): ...here.

2021-12-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Fix bug: Remove extraneous "http://" from @uref

	* xdgdirs.texi (Introduction): ...here,
	for "XDG Basedir Specification".

2021-12-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot int] Use https for gnuvola URL

	* configure.ac (AC_INIT): ...here, in 5th arg.

2015-05-24  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 2.2

	* configure.ac (AC_INIT): Bump version to "2.2" for release.

2015-05-24  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Index each LANG entry.

	* xdgdirs.texi (Invoking xdgdirs):
	Add @cindex for each LANG in ‘-l LANG’ table.

2015-05-24  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add support for ‘--lang yaml’.

	* xdgdirs.in (COPYRIGHT): Update years.
	(weirdness): Add entry for ‘yaml’.
	* v06: New file.
	* xdgdirs.texi (@copying): Update years in copyright notice.
	(Invoking xdgdirs): Add ‘@item yaml’.
	* Makefile.am (TESTS): Add v06.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 2.1

	* configure.ac (AC_INIT): Bump version to "2.1" for release.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Arrange for configure to instantiate homepage.

	* configure.ac (AC_INIT): Specify TARNAME and URL.
	* xdgdirs.in <check-hv>: Use "@PACKAGE_URL@".

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Include bug-report and home-page info in ‘--help’ output.

	* xdgdirs.in <check-hv>: ...here, after usage info.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Combine "check-hv" guard cond and ‘case’ selector expr.

	* xdgdirs.in <check-hv>: ...here.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Don't bother distributing file xdgdirs.

	It is overwritten by the configure script, anyway.

	* Makefile.am (EXTRA_DIST): Remove xdgdirs.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Distribute HTML and PDF documentation as well.

	* Makefile.am (EXTRA_DIST): Add xdgdirs.html, xdgdirs.pdf.

2013-09-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Support single-char option variants.

	* xdgdirs.in (qop): ...here.
	* xdgdirs.texi (Invoking xdgdirs): Update table.

2013-09-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 2.0

	* configure.ac (AC_INIT): Bump version to "2.0" for release.

2013-09-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Initial commit.

	Heavily revised from 1.0, posted to guile-sources 2013-09-05.

	* AUTHORS: New file.
	* Makefile.am: New file.
	* autogen.sh: New file.
	* configure.ac: New file.
	* v-fake: New file.
	* v-prep: New file.
	* v00: New file.
	* v01: New file.
	* v02: New file.
	* v03: New file.
	* v04: New file.
	* v05: New file.
	* xdgdirs.in: New file.
	* xdgdirs.texi: New file.


Copyright (C) 2013, 2021 Thien-Thi Nguyen

Copying and distribution of this file, with or without modification,
are permitted provided the copyright notice and this notice are preserved.
