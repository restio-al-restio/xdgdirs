#!/bin/sh
# autogen.sh
#
# Copyright (C) 2013, 2021 Thien-Thi Nguyen
#
# This file is part of xdgdirs.
#
# xdgdirs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# xgdirs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with xdgdirs.  If not, see <https://www.gnu.org/licenses/>.

set -ex
autoreconf --verbose --install --force
gnulib-tool --copy-file doc/INSTALL.UTF-8 INSTALL
gnulib-tool --copy-file doc/fdl.texi build-aux/fdl.texi
rm -f INSTALL~ build-aux/fdl.texi~

# autogen.sh ends here
